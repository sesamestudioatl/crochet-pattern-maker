# import the necessary packages
from sklearn.cluster import MiniBatchKMeans
import numpy as np
import argparse
import cv2

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
ap.add_argument("-s", "--scale", required=True, help="Scale the image before generating pattern", type=float)
ap.add_argument("-g", "--colored_grid", required=False, default = False, help="Keep color on grid", type = bool)
ap.add_argument("-c", "--clusters", required=True, type=int,
                help="# of clusters")
args = vars(ap.parse_args())

# load the image and grab its width and height
image = cv2.imread(args["image"])

percent_resize = args["scale"]
image = cv2.resize(image, (0,0), fx=percent_resize, fy=percent_resize)

(h, w) = image.shape[:2]

# convert the image from the RGB color space to the L*a*b*
# color space -- since we will be clustering using k-means
# which is based on the euclidean distance, we'll use the
# L*a*b* color space where the euclidean distance implies
# perceptual meaning
image = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)

# reshape the image into a feature vector so that k-means
# can be applied
image = image.reshape((image.shape[0] * image.shape[1], 3))

# apply k-means using the specified number of clusters and
# then create the quantized image based on the predictions
clt = MiniBatchKMeans(n_clusters=args["clusters"])
labels = clt.fit_predict(image)
quant = clt.cluster_centers_.astype("uint8")[labels]

# reshape the feature vectors to images
quant = quant.reshape((h, w, 3))
image = image.reshape((h, w, 3))
#print(image.shape[:2])
small = image
small_quant = quant

#print(small.shape[:2])

# convert from L*a*b* to RGB
small_quant = cv2.cvtColor(small_quant, cv2.COLOR_LAB2BGR)
small = cv2.cvtColor(small, cv2.COLOR_LAB2BGR)

cv2.imwrite("quantized-test.jpg", small_quant)

print('Final Image is of size: {0} X {1} X {2}'.format(str(int(h)), str(int(w)), '3'))
print("{0} x {1} stiches".format(str(int(h)), str(int(w))))
print("{0} stitches found".format(str(int(h*w) + h)))
# display the images and wait for a keypress
#cv2.imshow("image", np.hstack([small, small_quant]))
#cv2.waitKey(0)


#cv2.imshow("image", small_quant)
#cv2.waitKey(0)

im = small_quant
(y, x, z) = im.shape

output = [[]]
color_dict = {}
counter = 0
output = ""

print("")

for i in range(0, y):
    #print("i:" + str(i))
    if i == 0:
        outputline = "ch{0} with the following colors: {1}, turn, ch1"
    else:
        outputline = "sc{0} with the following colors: {1}, turn, ch1"
    color_instructions = ""

    if i % 2 == 1:

        start_iter = x - 1
        end_iter = -1
        step = -1
    else:
        start_iter = 0
        end_iter = x
        step = 1

    for j in range(start_iter, end_iter, step):
        #print(j)
        #print("ITER: " + str(j))
        r = im[i, j, 0]
        g = im[i, j, 1]
        b = im[i, j, 2]
        key = str(b) + "," + str(g) + "," + str(r)
        if not key in color_dict.keys():
            #print("Adding key: " + str(counter))
            color_dict[key] = str(chr(counter + 65))
            #print(str(chr(counter + 65)))
            counter += 1
        color_instructions += str(color_dict[key])
        #print(color_instructions)
        if i % 2 == 1:
            if j > 0:
                color_instructions += ", "
            if j <= 0:
                outputline = "Round {0}<br>".format(str(i+1)) + outputline.format(str(w), color_instructions)
                output += "<br> " + outputline + "<br>\n\n"
        else:
            if j < x - 1:
                color_instructions += ", "
            if j >= x - 1:
                outputline = "Round {0}<br>".format(str(i + 1)) + outputline.format(str(w), color_instructions)
                output += "<br> " + outputline + "<br>\n\n"
    #print(outputline)
    #break

legend_list = [None] * len(color_dict.keys())

if len(color_dict.keys()) > 26:
    raise Exception("More than 26 colors were found. Please decrease cluster count")

for key in color_dict.keys():
    int_val = ord(color_dict[key]) - 65
    legend_list[int_val] = """<p style="background-color:rgb({0})"> {1} : {0}""".format(key, color_dict[key])
#print("Legend")
# for value in legend_list:
#     print(value)

print("")
#out_final = "Photo:\n" + output + "\n" + """Border:
#sc around with border color"""
out_final = """<html>
<body>
<p>
Maki Crochet Photo
<br>
<br> {1} stitches
<br>
<img src="{0}" alt="" height="200" width="200">
<img src="{2}" alt="" height="200" width="200">
<br>
<br> Legend
<br>
""".format(args["image"] ,str(int(h*w) + h), "quantized-test.jpg")

for value in legend_list:
    out_final += value + "\n"

out_final += "</p>\n<p>" + output + "</p>"

out_final += """
<br>
<br>
<br> Border:
<br> sc around with border color
</p>
</body>
</html>"""


f = open("pattern.html", 'w')
f.write(out_final)
f.close()


print(color_dict.keys())
print(len(color_dict.keys()))


#recreate image
import scipy.misc
import numpy as np

# Image size
width = x
height = y
channels = 3

# Create an empty image
gridsize = 30
img = np.zeros((height, width, channels), dtype=np.uint8)
img_big = np.zeros((height * gridsize, width * gridsize, channels), dtype=np.uint8)


import numpy as np
def getGlyph(letter, color = (255, 255, 255), save = False):
    glyph = np.zeros((gridsize, gridsize, channels), dtype=np.uint8)
    font = cv2.QT_FONT_NORMAL
    #print(cv2.getTextSize(letter, font, 1, 1))
    cv2.putText(glyph, letter, (3, 3), font, 1, color, 1, cv2.LINE_AA, bottomLeftOrigin=True)
    glyph = cv2.flip(glyph, 0)
    glyph[np.where((glyph == [0, 0, 0]).all(axis = 2))] = [0, 33, 166]
    glyph[np.where((glyph != [0, 33, 166]).all(axis = 2))] = [0, 0, 0]
    glyph[np.where((glyph == [0, 33, 166]).all(axis = 2))] = [255, 255, 255]

    if save:
        scipy.misc.imsave("temp/" + letter + ".jpg", glyph)
    return glyph

#getGlyph('J')
# Set the RGB values
print(image.shape)
for y in range(img.shape[0]):
    for x in range(img.shape[1]):
        #print(x)
        r, g, b = im[y, x, 0], im[y, x, 1], im[y, x, 2]
        img[y][x][0] = b
        img[y][x][1] = g
        img[y][x][2] = r
        key = str(b) + "," + str(g) + "," + str(r)
        letter = color_dict[key]
        #print("Letter: " + letter)
        color = (255, 255, 255)
        glyph = getGlyph(letter, color)

        retain_color = args['colored_grid']

        #print(retain_color)
        for i in range(0,gridsize):
            for j in range(0, gridsize):
                inverse_x = i#gridsize - i - 1
                inverse_y = j#gridsize - j - 1
                if retain_color:
                    if glyph[inverse_x][inverse_y][0] != 255:
                        img_big[y * gridsize + i][x * gridsize + j][0] = glyph[inverse_x][inverse_y][0]
                        img_big[y * gridsize + i][x * gridsize + j][1] = glyph[inverse_x][inverse_y][1]
                        img_big[y * gridsize + i][x * gridsize + j][2] = glyph[inverse_x][inverse_y][2]
                    else:
                        img_big[y * gridsize + i][x * gridsize + j][0] = b
                        img_big[y * gridsize + i][x * gridsize + j][1] = g
                        img_big[y * gridsize + i][x * gridsize + j][2] = r
                else:
                    img_big[y * gridsize + i][x*gridsize + j][0] = glyph[inverse_x][inverse_y][0]
                    img_big[y * gridsize + i][x*gridsize + j][1] = glyph[inverse_x][inverse_y][1]
                    img_big[y * gridsize + i][x*gridsize + j][2] = glyph[inverse_x][inverse_y][2]



# Grid lines at these intervals (in pixels)
# dx and dy can be different
dx, dy = gridsize, gridsize

# Custom (rgb) grid color
grid_color = [240, 128, 128]

# Modify the image to include the grid
img_big[:, ::dy, :] = grid_color
img_big[::dx, :, :] = grid_color

# Display the image
# import matplotlib.pyplot as plt
# plt.imshow(np.uint8(img_big))
# plt.show()

# Save the image
scipy.misc.imsave("image.jpg", img)
scipy.misc.imsave("grid_maki.jpg", img_big)

