import cv2

color_count = 20

im = cv2.imread("quantized-test.jpg")
print(type(im))
print(im.shape)

(y, x, z) = im.shape

output = [[]]
color_dict = {}
counter = 0

for i in range(0, y):
    for j in range(0, x):
        r = im[i, j, 0]
        g = im[i, j, 1]
        b = im[i, j, 2]
        key = str(r) + "," + str(g) + "," + str(b)
        if not key in color_dict.keys():
            color_dict[key] = counter
            counter += 1

print(color_dict.keys())
print(len(color_dict.keys()))

